// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

// See `config.ron` for comments on some of the more
// mysterious fields.

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct GameConfig {
    pub arena_height: f32,
    pub arena_width: f32,
    pub paddle_height: f32,
    pub paddle_width: f32,
    pub paddle_accel: f32,
    pub paddle_max_accel: f32,
    pub paddle_english: f32,
    pub paddle_thwap: f32,
    pub paddle_sweetrange: f32,
    pub ball_size: f32,
    pub ball_speed: f32,
    pub ball_speed_max: f32,
    pub ball_speed_min: f32,
    pub ball_dirn_x: f32,
    pub ball_dirn_y: f32,
    pub ball_max_angle: f32,
    pub frame_rate: u32,
}

impl Default for GameConfig {
    fn default() -> Self {
        let ball_speed = 75.0;
        Self {
            arena_height: 100.0,
            arena_width: 100.0,
            paddle_height: 16.0,
            paddle_width: 4.0,
            paddle_accel: 0.25,
            paddle_max_accel: 3.0,
            paddle_english: 0.4,
            paddle_thwap: 0.7,
            paddle_sweetrange: 0.7,
            ball_size: 4.0,
            ball_speed: ball_speed,
            ball_speed_max: 3.0 * ball_speed,
            ball_speed_min: ball_speed,
            ball_dirn_x: 1.0,
            ball_dirn_y: 0.0,
            ball_max_angle: 0.9,
            frame_rate: 120,
        }
    }
}
