// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct BounceSystem;

impl<'s> System<'s> for BounceSystem {
    type SystemData = (
        WriteStorage<'s, Ball>,
        ReadStorage<'s, Paddle>,
        ReadStorage<'s, Transform>,
        Read<'s, GameConfig>,
    );

    fn run(
        &mut self,
        (mut balls, paddles, transforms, config): Self::SystemData,
    )
    {
        for (ball, transform) in (&mut balls, &transforms).join() {
            let tr = transform.translation();
            let ball_x = tr.x;
            let ball_y = tr.y;

            for (paddle, transform) in (&paddles, &transforms).join() {
                let tr = transform.translation();
                let paddle_x = tr.x;
                let paddle_y = tr.y;

                // A ball can't hit a paddle from the "back".
                let dx = ball.dirn[0];
                match paddle.side {
                    Side::Left if dx >= 0.0 => continue,
                    Side::Right if dx <= 0.0 => continue,
                    _ => (),
                }

                // Return the distance from the paddle
                // center and the paddle bound in the given
                // coordinate.
                let s = ball.size;   // XXX Do not borrow ball.
                let in_paddle = |bc, pc, span| {
                    let bound: f32 = span * 0.5 + s;
                    let diff: f32 = pc - bc;
                    let bounce = diff <= bound && diff >= -bound;
                    (diff, bound, bounce)
                };

                // A ball cannot hit a paddle if it is to
                // the left or right.
                let (_, _, bounce) =
                    in_paddle(ball_x, paddle_x, paddle.width);
                if !bounce {
                    continue;
                }

                // A ball cannot hit a paddle if it is above
                // or below.
                let (diff, bound, bounce) =
                    in_paddle(ball_y, paddle_y, paddle.height);
                if !bounce {
                    continue;
                }

                // The ball has hit the paddle and must
                // bounce.  Reflect the x direction, and
                // adjust the y direction according to the
                // hit position on the paddle.
                let ratio = diff / bound;
                let dx = -ball.dirn[0];
                let dy = ball.dirn[1];
                let paddle_english = config.paddle_english * ratio;

                // Set the new ball direction.
                ball.dirn =
                    Vector2::new(dx, dy - paddle_english);
                let angle = ball.dirn[1] / ball.dirn[0].abs();
                // XXX max_angle ~== atan(pi/3.0). We
                // don't want to have more than a few
                // bounces.
                let max_angle = config.ball_max_angle;
                if angle > max_angle {
                    ball.dirn =
                        Vector2::new(1.0, max_angle);
                } else if angle < -max_angle {
                    ball.dirn =
                        Vector2::new(1.0, -max_angle);
                }
                ball.dirn = ball.dirn.normalize();

                // Accelerate or decelerate the ball.
                ball.speed *=
                    1.0 + config.paddle_thwap *
                    (config.paddle_sweetrange - ratio.abs());
                ball.speed =
                    ball.speed
                    .min(config.ball_speed_max)
                    .max(config.ball_speed_min);

                // Break the paddle loop (so that one paddle
                // is applied per ball per turn).
                break;
            }

            // If the ball is about to leave the arena above or
            // below, reflect it back.
            let at_bottom =
                ball_y >= config.arena_height - ball.size &&
                ball.dirn[1] > 0.0;
            let at_top =
                ball_y <= ball.size && ball.dirn[1] < 0.0;
            if at_bottom || at_top {
                ball.dirn[1] = -ball.dirn[1];
            }
        }
    }
}
