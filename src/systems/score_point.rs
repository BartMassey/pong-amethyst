// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct ScorePointSystem;

impl<'s> System<'s> for ScorePointSystem {
    type SystemData = (
        WriteStorage<'s, Ball>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, UiText>,
        Write<'s, ScoreBoard>,
        ReadExpect<'s, ScoreTexts>,
        Read<'s, GameConfig>
    );

    fn run(&mut self,
           (mut balls,
            mut transforms,
            mut ui_text,
            mut scores,
            score_texts,
            config,
           ): Self::SystemData)
    {
        for (ball, transform) in (&mut balls, &mut transforms).join() {
            let ball_x = transform.translation().x;

            let margin = 0.5 * config.paddle_width;
            let side_won = match ball_x {
                x if x <= margin => Some(Left),
                x if x >= config.arena_width - margin => Some(Right),
                _ => None
            };
            side_won.map(|side| {
                // Player scored.
                let new_score = scores.increase(side, 1);
                let text = score_texts.texts[1 - side as usize];
                if let Some(text) = ui_text.get_mut(text) {
                    text.text = new_score.to_string();
                }

                // Start the ball over.
                let t = transform.translation_mut();
                let baseline = 4.0 * config.paddle_width;
                let (nx, nd) = match side {
                    Left => (baseline, 1.0),
                    Right => (config.arena_width - baseline, -1.0),
                 };
                t.x = nx;
                t.y = config.arena_height / 2.0;
                ball.dirn[0] = nd;
                ball.dirn[1] = 0.0;
                ball.speed = config.ball_speed;
            });
        }
    }
}
