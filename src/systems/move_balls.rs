// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct MoveBallsSystem;

impl<'s> System<'s> for MoveBallsSystem {
    type SystemData = (
        ReadStorage<'s, Ball>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>,
    );

    fn run(&mut self, (balls, mut locals, time): Self::SystemData) {
        let t = time.delta_seconds();
        for (ball, local) in (&balls, &mut locals).join() {
            let d = ball.speed * t;
            let t = local.translation_mut();
            t.x += ball.dirn[0] * d;
            t.y += ball.dirn[1] * d;
        }
    }
}
