// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct MovePaddlesSystem;

impl<'s> System<'s> for MovePaddlesSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, PaddleAccel>,
        ReadStorage<'s, Paddle>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, GameConfig>
    );

    fn run(&mut self,
           (mut transforms,
            mut paddle_accels,
            paddles,
            input,
            config): Self::SystemData)
    {
        let paddle_names = ["left_paddle", "right_paddle"];
        for (paddle, paddle_accel, transform) in
            (&paddles, &mut paddle_accels, &mut transforms).join()
        {
            let paddle_name = paddle_names[paddle.side as usize];
            let movement = input.axis_value(paddle_name);
            let movement = paddle_accel.accelerate(movement);
            if let Some(movement) = movement {
                transform.translation_mut().y =
                    (transform.translation().y + movement)
                    .min(config.arena_height - paddle.height * 0.5)
                    .max(paddle.height * 0.5);
            }
        }
    }
}
