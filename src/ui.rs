// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

/// ScoreBoard contains the actual score data
#[derive(Default)]
pub struct ScoreBoard {
    pub scores: [i32; 2],
}

impl ScoreBoard {
    pub fn increase(&mut self, side: Side, amount: i32) -> i32 {
        let i = side as usize;
        self.scores[i] =
            (self.scores[i] + amount).min(999).max(-99);
        self.scores[i]
    }
}

/// ScoreText contains the ui text components that display the score
pub struct ScoreTexts {
    pub texts: [Entity; 2],
}

impl ScoreTexts {
    pub fn initialise(world: &mut World) {
        let mut p = PathBuf::new();
        p.push("fonts");
        p.push("pong.ttf");
        let font_path =
            resource_path(p).unwrap();
        let font =
            world.read_resource::<Loader>().load(
                font_path,
                TtfFormat,
                Default::default(),
                (),
                &world.read_resource(),
            );

        let texts: Vec<Entity> =
            [("P1", -50.), ("P2", 50.)]
            .into_iter()
            .map(|&(name, x): &(&str, f32)| {
                UiTransform::new(
                    name.to_string(),
                    Anchor::TopMiddle,
                    x, -50., 1., 200., 50.,
                )
            })
            .map(|tx| {
                let ui_text = UiText::new(
                    font.clone(),
                    "0".to_string(),
                    [1., 1., 1., 1.],
                    50.,
                );
                world
                    .create_entity()
                    .with(tx)
                    .with(ui_text)
                    .build()
            })
            .collect();

        world.add_resource(ScoreTexts {
            texts: [texts[0], texts[1]]
        });
    }
}
