// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

mod components;
mod pong;
mod prelude;
mod systems;
mod ui;
mod config;

pub use self::components::*;
pub use self::pong::*;
pub use self::systems::*;
pub use self::ui::*;
pub use self::config::*;

pub use self::prelude::*;

use amethyst::{LoggerConfig, LogLevelFilter};
use amethyst::core::{
    transform::TransformBundle,
    frame_limiter::FrameRateLimitStrategy,
};
use amethyst::renderer::{
    DisplayConfig, Pipeline, DrawFlat2D, RenderBundle, Stage
};
use amethyst::input::InputBundle;

use std::path::{Path, PathBuf};
use std::time::Duration;

use amethyst_utils::app_root_dir::*;

pub fn resource_path<P>(tail: P) -> amethyst::Result<String>
    where P: AsRef<Path>
{
    let mut p = PathBuf::new();
    p.push("resources");
    p.push(tail);
    match application_dir(p) {
        Ok(p) => Ok(p.to_str().unwrap().to_string()),
        Err(e) => Err(e.into()),
    }
}

fn main() -> amethyst::Result<()> {
    let mut logger_config: LoggerConfig = Default::default();
    logger_config.level_filter = LogLevelFilter::Warn;
    amethyst::start_logger(logger_config);

    let config_path = resource_path("config.ron")?;
    let config = GameConfig::load(&config_path);

    let display_path = resource_path("display_config.ron")?;
    let mut display_config = DisplayConfig::load(&display_path);

    // Adjust the size and aspect ratio of the window.
    let dimensions = display_config.dimensions;
    let aw = config.arena_width;
    let ah = config.arena_height;
    display_config.dimensions = match dimensions {
        None => Some((aw.floor() as u32, ah.floor() as u32)),
        Some((dw, dh)) => {
            let sw = dw as f32 / aw as f32;
            let sh = dh as f32 / ah as f32;
            let scale = f32::max(sw, sh);
            let dw = (aw * scale).floor() as u32;
            let dh = (ah * scale).floor() as u32;
            Some((dw, dh))
        }
    };

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.002, 0.05, 0.002, 1.0], 1.0)
            .with_pass(DrawFlat2D::new())
            .with_pass(DrawUi::new()));
    let render_bundle = RenderBundle::new(pipe, Some(display_config))
        .with_sprite_sheet_processor();

    let binding_path = resource_path("bindings_config.ron")?;
    let input_bundle = InputBundle::<String, String>::new()
        .with_bindings_from_file(&binding_path)?;

    let game_data =
        GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(input_bundle)?
        .with_bundle(UiBundle::<String, String>::new())?
        .with(systems::MovePaddlesSystem, "paddle_system",
              &["input_system"])
        .with(systems::MoveBallsSystem, "ball_system", &[])
        .with(systems::BounceSystem, "collision_system",
              &["paddle_system", "ball_system"])
        .with(systems::ScorePointSystem, "score_point_system",
              &["ball_system"])
        .with_barrier()
        .with_bundle(render_bundle)?;

    let fr = config.frame_rate;
    let ns = 1.0e9 / fr as f64;
    let frame_time = Duration::from_nanos(ns.ceil() as u64);
    let max_frame_rate = 2 * fr;

    let root = application_root_dir()?;
    let mut game = Application::build(&root, Pong)?
        .with_resource(config)
        .with_frame_limit(FrameRateLimitStrategy::Yield, max_frame_rate)
        .with_fixed_step_length(frame_time)
        .build(game_data)?;
    game.run();

    Ok(())
}
