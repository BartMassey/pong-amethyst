// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct Pong;

impl SimpleState for Pong {

    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        let sprite_sheet_handle = load_sprite_sheet(world);
        world.register::<Paddle>();
        Paddle::initialise(world, sprite_sheet_handle.clone());
        world.register::<Ball>();
        Ball::initialise(world, sprite_sheet_handle.clone());
        initialise_camera(world);
        ScoreTexts::initialise(world);
    }
}

fn initialise_camera(world: &mut World) {
    let config = world.read_resource::<GameConfig>().clone();
    let mut transform = Transform::default();
    transform.translation_mut().z = 1.0;
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            config.arena_width,
            config.arena_height,
            0.0,
        )))
        .with(transform)
        .build();
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Side {
    Left = 0,
    Right = 1,
}
pub use self::Side::*;

fn texture_path(tail: &str) -> String {
    let mut p = PathBuf::new();
    p.push("textures");
    p.push(tail);
    resource_path(p).unwrap()
}

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        let tpng_path = texture_path("pong_spritesheet.png");
        loader.load(
            tpng_path,
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    let tron_path = texture_path("pong_spritesheet.ron");
    loader.load(
        // Here we load the associated ron file
        tron_path,
        SpriteSheetFormat,
        // We pass it the handle of the texture we want it to use
        texture_handle,
        (),
        &sprite_sheet_store,
    )
}
