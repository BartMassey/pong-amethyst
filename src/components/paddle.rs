// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Paddle {
    pub fn new(side: Side, width: f32, height: f32) -> Self {
        Paddle {
            side,
            width: width,
            height: height,
        }
    }

    /// Initialises one paddle on the left, and one paddle on the right.
    pub fn initialise(world: &mut World, sprite_sheet: SpriteSheetHandle)
    {
        let config = world.read_resource::<GameConfig>().clone();
        let y = config.arena_height / 2.0;
        let side_x_translations = [
            config.paddle_width,
            config.arena_width - config.paddle_width,
        ];

        for &side in [Left, Right].into_iter() {
            // Correctly position the paddle.
            let mut side_transform = Transform::default();
            let t = side_transform.translation_mut();
            t.x = side_x_translations[side as usize];
            t.y = y;
            t.z = 0.0;

            // Assign the sprite for the paddle
            let sprite_render = SpriteRender {
                sprite_sheet: sprite_sheet.clone(),
                sprite_number: 0, // paddle is the first sprite in the sprite_sheet
            };

            // Create a "plank" entity.
            let mut builder = world
                .create_entity()
                .with(Paddle::new(
                    side,
                    config.paddle_width,
                    config.paddle_height,
                ))
                .with(PaddleAccel::new(
                    config.paddle_accel,
                    config.paddle_max_accel,
                ))
                .with(side_transform)
                .with(sprite_render.clone());
            if side == Right {
                builder = builder.with(Flipped::Horizontal);
            }
            builder.build();
        }
    }
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}

