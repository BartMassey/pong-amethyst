// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct Ball {
    pub size: f32,
    pub speed: f32,
    pub dirn: Vector2<f32>,
}

impl Ball {
    pub fn new(size: f32, speed: f32, dirn_x: f32, dirn_y: f32) -> Ball {
        Ball {
            size,
            speed,
            dirn: Vector2::new(dirn_x, dirn_y).normalize(),
        }
    }

    /// Initialises a ball at the center of the arena.
    pub fn initialise(world: &mut World, sprite_sheet: SpriteSheetHandle)
    {
        let config = world.read_resource::<GameConfig>().clone();

        // Correctly position the paddle.
        let mut posn_transform = Transform::default();
        let t = posn_transform.translation_mut();
        t.x = config.arena_width / 2.0;
        t.y = config.arena_height / 2.0;
        t.z = 0.0;

        // Assign the sprite for the paddle
        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet,
            sprite_number: 1, // ball is the first sprite in the sprite_sheet
        };

        // Create a "ball" entity.
        world
            .create_entity()
            .with(Ball::new(
                config.ball_size,
                config.ball_speed,
                config.ball_dirn_x,
                config.ball_dirn_y,
            ))
            .with(posn_transform)
            .with(sprite_render)
            .build();
    }
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}
