// Copyright © 2018 Bart Massey and The Amethyst Project Developers
// [This code is licensed "Apache v2.0" or "MIT" at your option.]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

use crate::*;

pub struct PaddleAccel {
    pub accel: Option<f32>,
    pub paddle_accel: f32,
    pub paddle_max_accel: f32,
}

impl PaddleAccel {
    pub fn new(paddle_accel: f32, paddle_max_accel: f32) -> PaddleAccel {
        PaddleAccel{
            accel: None,
            paddle_accel,
            paddle_max_accel,
        }
    }

    pub fn accelerate(&mut self, movement: Option<f64>) -> Option<f32> {
        let movement = movement.map(|m| m as f32);
        match movement {
            Some(mut motion) if motion != 0.0 => {
                let dirn = f32::signum(motion);
                self.accel = self.accel
                    .and_then(|mut accel| {
                        let adirn = f32::signum(accel);
                        if adirn == dirn {
                            let aspeed = f32::abs(accel);
                            motion *= aspeed;
                            accel += dirn * self.paddle_accel;
                            accel = f32::min(self.paddle_max_accel, accel);
                            accel = f32::max(-self.paddle_max_accel, accel);
                            Some(accel)
                        } else {
                            None
                        }
                    })
                    .or_else(|| {
                        Some(dirn * self.paddle_accel)
                    });
                Some(motion)
            },
            _ => {
                self.accel = None;
                None
            },
        }
    }
}

impl Component for PaddleAccel {
    type Storage = DenseVecStorage<Self>;
}
