#!/bin/sh
# Copyright © 2018 Bart Massey and The Amethyst Project Developers
# [This code is licensed "Apache v2.0" or "MIT" at your option.]
# Please see the file LICENSE in the source
# distribution of this software for license terms.

# This install script is really specific to my personal
# installation.  Something more general would be a better
# idea.

INSTALL_BASE=/local/apps
INSTALL_NAME=pong-amethyst-git
INSTALL_LINK=`echo $INSTALL_NAME | sed 's/-[^-]*$//'`
INSTALL_DIR=$INSTALL_BASE/$INSTALL_NAME
( cd $INSTALL_BASE &&
  rm -rf $INSTALL_NAME $INSTALL_LINK
  mkdir $INSTALL_NAME &&
  ln -s $INSTALL_NAME $INSTALL_LINK &&
  cd $INSTALL_NAME &&
  mkdir bin &&
  cd bin &&
  ln -s ../pong pong ) &&
cargo build --release &&
cp target/release/pong $INSTALL_DIR/ &&
cp -a resources $INSTALL_DIR/
