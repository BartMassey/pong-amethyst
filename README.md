# Pong, Amethyst Tutorial Style
Copyright (c) 2018 Bart Massey and The Amethyst Project Developers

This repository contains my Rust / Amethyst implementation
of the tutorial Pong application from the
[Amethyst](http://amethyst.rs) Book. This implementation
lacks some of the features of the example version of Pong
distributed with Amethyst, such as sound. The emphasis here
was on turning it into a fun-to-play game, and on cleaning
up the code as much as reasonably feasible. I learned a lot
while building it.

## Build, Run, and Install

You must have an installation of stable Rust (2018 Edition
or better). Then just type `cargo build --release` to build
the application and wait a long time.

You can run the application with `cargo run --release` (from
the source root directory only).

The application may be installed anywhere by just copying it
from `target/release/pong`, but it needs to find its
`resource` directory in the same directory as the binary, so
copy that too.

## Controls

Player 1 uses the "w" and "s" keys to move their
paddle. Player 2 uses the "↑" and "↓" (up and down) arrow
keys to move their paddle.

## Features

* "Paddle Acceleration": Paddle movement speed will increase
  while it is moving in a given direction. This allows fast
  movement and fine adjustment of the paddles.

* "Paddle English": The ball angle will be modified by where
  on the paddle it is hit.

* "Paddle Thwap": The ball speed will increase or decrease
  depending on where on the paddle it is hit.

## License

This code is licensed under the "Apache License Version 2.0"
or the "MIT license" at your option. Please see the file
`LICENSE` in this distribution for license terms.
